import numpy as np
import cairoshop as cs
import time
from numpy.random import default_rng
rng = default_rng()


def rotation(r, omega, t):
    return (np.array([r, r])*np.array([np.cos(omega*t), np.sin(omega*t)])).T


#rotation = np.vectorize(rotation)


def overlay_rotations(r_array, omega_array, dt, tmax):
    for t in np.arange(0, tmax, dt):
        points = rotation(r_array, omega_array, t)
        yield np.sum(points, axis=0)


def generate_r(rmax, n):
    r_array = []
    for i in range(n-1):
        radius = rng.integers(np.minimum(50, rmax-1), rmax)
        rmax -= radius
        r_array.append(radius)
    r_array.append(rmax)
    rng.shuffle(r_array)
    return np.array(r_array)


def generate_omega(n):
    omega0 = rng.uniform(0, 1)*np.pi
    omega_array = [omega0]
    omega_step = rng.integers(1, 5)
    for i in range(n-1):
        omega = rng.integers(1, omega_step*15)*omega0/omega_step
        omega_array.append(omega)
    rng.shuffle(omega_array)
    return np.array(omega_array)


def round_omega(omega_array, r_array):
    index = np.argmin(r_array)
    index = rng.integers(0, len(omega_array))
    omega_array[index] = np.around(omega_array[index], decimals=1)


if __name__ == "__main__":

    for n in range(10):
        dim = 2000
        img = cs.Image(dim, dim)
        img.scale(.5, .5)
        img.bg.color_fill(1, 1, 1)
        l = img[0]
        l.translate(2000, 2000)
        l.set_source_rgba(0, 0, 0, .1)
        l.set_line_width(5)
        l.set_line_cap(cs.cairo.LineCap.ROUND)

        # r_array = np.array([500, 500, 100, 900])
        # omega_array = np.array([.20*np.pi, 4*np.pi, np.around(.5*np.pi, decimals=1), np.pi])
        n_rotations = rng.integers(3, 6)
        r_array = generate_r(dim, n_rotations)
        omega_array = generate_omega(n_rotations)
        round_omega(omega_array, r_array)

        print(r_array)
        print(omega_array)

        points = overlay_rotations(r_array, omega_array, .005, 1000*np.pi)
        start = next(points)
        l.move_to(*start)

        for i, point in enumerate(points):
            l.line_to(*point)
            #l.circle(*point, 2)
            #l.stroke()
            if np.allclose(point, start, atol=1):
                break
            if i % 10 == 0:
                l.stroke()
                l.line_to(*point)

        l.stroke()

        img.draw(f"./images/{time.time()}_{n}.png")
